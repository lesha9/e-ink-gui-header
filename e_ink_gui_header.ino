
// DIN	SPI MOSI
// CLK	SPI SCK
// CS	SPI chip select (Low active)
// DC	Data/Command control pin (High for data, and low for command)
// RST	External reset pin (Low for reset)
// BUSY	Busy state output pin (Low for busy)

#include "DEPG0150BxS810FxX_BW.h"

#include "picture.h"
#include "e_ink_display.h"
#include "backg.h"
#include <SPI.h> ///Это обязательно 


/*=======================  Пины для дисплея =======================*/

  #define RST_PIN -1
  #define DC_PIN 12 // miso
  #define CS_PIN 15 // cs
  #define BUSY_PIN 27
  #define CLK_PIN 14 // cls

DEPG0150BxS810FxX_BW epd154bw(RST_PIN, DC_PIN, CS_PIN, BUSY_PIN, CLK_PIN); // reset_pin, dc_pin, cs_pin, busy_pin, clk_pin SPI_NUM_0, FREQUENCY

unsigned char img[40000];
Paint pt(img, 0, 0);



unsigned long time_start_ms;
unsigned long time_now_s;

#define COLORED 0
#define UNCOLORED 1

int draw_menu(String left_text, String middle_text, String right_text)
{
  pt.SetWidth(20);
  pt.SetHeight(200);
  pt.Clear(COLORED);
  pt.DrawStringAt(1, 4, left_text.c_str(), &Font12, UNCOLORED);
  pt.DrawStringAt(100 - middle_text.length() / 2 * 7, // X центрального текста, 100 - половина ширины экрана, middle_text.length() / 2 - половина символов текста, 7 - ширина одного символа
                  4, middle_text.c_str(), &Font12, UNCOLORED);
  
  pt.DrawStringAt(200 - 7 * right_text.length(), //координата x заряда батареи, 200 - ширина экрана, 7 - ширина 1 символа Font12, и коллечество символов
                  4, right_text.c_str(), &Font12, UNCOLORED);
                  
  epd154bw.SetFrameMemory(pt.GetImage(), 0, 0, pt.GetWidth(), pt.GetHeight());
  return 0;
}


void setup()
{

	pinMode(BUSY_PIN, INPUT);   //инициализация Bysy пина на вход - принимать от дисплея статус  
	SPI.begin(14, 12, 13, 15);  //В этом надо разобраться ибо epd154bw уже содержит Инициализацию, но без єтого не работает.
                                // Скорее всего потому что либа работает с VSPI по умолчанию, а это пины от HSPI

	epd154bw.EPD_Init(); // Electronic paper initialization
	delay(1000);
  epd154bw.DisplayPartBaseImage(gImage_backg);

	epd154bw.HalLcd_Partial();   //Конкретно инициализация частичного обновления экрана.
  pt.SetRotate(ROTATE_270);    //Поворачиваем правильно экран - єто важно. Пины дисплея должны бысть с правой стороны 
  

  draw_menu("19:26","Test text","100%");
	for (int i = 20; i <= 180; i += 20)
	{
    
    pt.SetWidth(20);
    pt.SetHeight(200);
    pt.Clear(UNCOLORED);
    pt.DrawStringAt(1, 4, String(i).c_str(), &Font12, COLORED); //Нужный текст закончится в позицию х1 у1 в области pt pt.SetWidth(23); pt.SetHeight(200);
    epd154bw.SetFrameMemory(pt.GetImage(), i, 0, pt.GetWidth(), pt.GetHeight());   //Помещение обьекта pt в БУФЕР
	}
  epd154bw.HalLcd_Partial_Update();   //обновление экрана без полного обновления.


	delay(5000);
}
void loop()
{
	delay(400);
}
